package com.decisionbrain.test;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestClassTest {

    @Test
    void helloWorld() {
        TestClass testObject = new TestClass();
        assertDoesNotThrow(testObject::helloWorld);
    }
}